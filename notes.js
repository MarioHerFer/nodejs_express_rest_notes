var notes = {};//objeto
var names = [];//array

exports.list = function(req, res) {
  res.json(Object.keys(notes));     
}

exports.get = function(note_name, req, res) {
  if (!notes.hasOwnProperty(note_name)) {
    res.status(404);
    res.end();
    console.error(note_name + " NOT FOUND!");
  } else {
    res.json(notes[note_name]);
  }
};

exports.insert = function(note_name, req, res) {
  if (notes.hasOwnProperty(note_name)) {
    res.status(409);
    return console.error(note_name + " ALREADY THERE!");
  } else {
    if (!req.body.content) {
      res.status(422);
      res.end();
      return console.error(note_name + " INCORRECT BODY JSON!");
    }
    //
    var ps = names.length;
    names[ps] = note_name;
    //
    notes[note_name] = { content: req.body.content, inserted: new Date() };
    res.end();
  }
};

exports.upsert = function(note_name, req, res) {
  if (notes.hasOwnProperty(note_name)) {
    notes[note_name] = { content: req.body.content, modified: new Date() };
    res.status(201);
    res.end();
  } else {
    this.insert(note_name, req, res);
  }
};

//
exports.delete = function(note_name, req, res) {
  if (!notes.hasOwnProperty(note_name)) {
    res.status(404);
    res.end();
    console.error(note_name + " NOT FOUND!");
  }else{
    delete notes[note_name];
    for(var i = 0; i < names.length; i++){
      if( names[i] == note_name){
        names.splice(i,1);
      }
    }
    res.end();
    console.log(names)
  }
};

exports.getContent = function(Scontent, req, res) {
  var found = false;
  for(var i = 0; i < names.length; i++){
    if(notes[names[i]].content.search(Scontent) >= 0){
      res.json(notes[names[i]]);
      found = true;
    }
  }
  if(!found){
    res.status(404);
    console.error(Scontent + " NOT FOUND");
  }  
  res.end();
};

